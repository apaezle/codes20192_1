package com.sigmotoa.codes.workshop;

import java.util.Arrays;

/**
 * @author sigmotoa
 *
 *         This class contains some popular brain games with numbers
 */
public class Agility {

	// bigger than

	// Show if the first number is bigger than the second
	public static boolean biggerThan(String numA, String numB) {
		boolean comparisonNumbers = (Double.parseDouble(numA) > Double.parseDouble(numB)) ? true : false;
		return comparisonNumbers;
	}

	// Sort from bigger the numbers an show in array
	public static int[] order(int numA, int numB, int numC, int numD, int numE) {
		int[] numbersOrder = {numA, numB, numC, numD, numE};
		Arrays.sort(numbersOrder);
		return numbersOrder;
	}

	// Look for the smaller number of the array
	public static double smallerThan(double array[]) {
		Arrays.sort(array);
		return array[0];
	}

	// Palindrome number is called in Spanish capicúa
	// The number is palindrome
	public static boolean palindromeNumber(Integer numA) {
		String numString = Integer.toString(numA);
		StringBuilder sb = new StringBuilder(numString);
		String cadenaInvertida = sb.reverse().toString();

		return cadenaInvertida.equals(numString);
	}

	// the word is palindrome
	public static boolean palindromeWord(String word) {
		StringBuilder sb = new StringBuilder(word);
		String cadenaInvertida = sb.reverse().toString();

		return cadenaInvertida.equals(word);
	}

	// Show the factorial number for the parameter
	public static int factorial(int numA) {
		int result = 1;
		
		for(int i = 1; i <= numA; i++) {
			result *= i;
		}
		
		return result;
	}

	// is the number odd
	public static boolean isOdd(byte numA) {
		return (numA % 2 == 0) ? false : true;
	}

	// is the number prime
	public static boolean isPrimeNumber(int numA) {
		int aux = 0;

		if(numA < 0)
			return false;

		for(int i = 1; i <= numA; i++) {
			if(numA % i == 0) {
				aux++;
			}
		}

		return aux == 2;
	}

	// is the number even
	public static boolean isEven(byte numA) {
	    return (numA % 2 == 0) ? true : false;
	}

	// is the number perfect
	public static boolean isPerfectNumber(int numA) {
		int resultado = 0;


		for(int i = 1; i < numA; i++){
			if(numA % i == 0){
				resultado += i;
			}
		}

	    return (resultado == numA) ? true : false;
	}

	// Return an array with the fibonacci sequence for the requested number
	public static int[] fibonacci(int numA) {
		int[] fibonacciArray = new int[numA];
		fibonacciArray[0] = 0;
		fibonacciArray[1] = 1;

		for(int i = 2; i < fibonacciArray.length; i++){
			fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
		}

		return fibonacciArray;
	}

	// how many times the number is divided by 3
	public static int timesDividedByThree(int numA) {
		return numA / 3;
	}

	// The game of fizzbuzz
	/**
	 * If number is divided by 3, show fizz If number is divided by 5, show buzz If
	 * number is divided by 3 and 5, show fizzbuzz in other cases, show the number
	 */
	public static String fizzBuzz(int numA) {
		if((numA % 3 == 0) && (numA % 5 == 0)){
			return "FizzBuzz";
		}else if (numA % 3 == 0){
			return  "Fizz";
		}else if(numA % 5 == 0){
			return "Buzz";
		}

		return Integer.toString(numA);
	}
}
